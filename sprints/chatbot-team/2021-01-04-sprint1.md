Sprint Plan 1: 

Maria:
- ~~Maya Bot: Send in advance the last $5000 invoice~~
- ~~Maya Bot: Write a maintenance retainer proposal~~ 
- Maya Bot: All the stuff from previous Sprint
  - Implement Chatbase Integration 
  - Debug free text logging 
  - Fix NLU (put language recognition first)
- Maya Bot: Meet with Nepal Team 
- Regulationship: Finalize the Regulationship proposal 
- Regulationship: Onboard Maya on Botpress 
- ~~Syndee: Send information to Gemma~~ 
- Add Maya user to Gsuite and share calendar


Maya
- Maya Bot: Finish connecting new intents in English and Nepali      **In progress** 
- Maya Bot: Implement quiz sharing in Safe Migration quiz            **DONE** 
- GirlEngage Proposal and budget (send draft by the end of week 1)   **Waiting for Hobson's approval** 
- Regulationship: Get comfortable with Botpress 
- Write a draft of QA list (possible actions)
- Check if "Share on Timeline button" is causing troub               **DONE - Nope.** 

Ideas: 
- No sarcasm, no slang, 
- Maybe Grammarly is an API? 
