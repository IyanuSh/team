# Mind Hacks

## Power Hour

Maybe you're having trouble making progress on a tough problem.
Or perhaps you're having trouble staying motivated and focused.
A power hour can help you "keep your butt in the chair" to be more productive and churn out some code or copy or whatever it is you're trying to get done.

1. Find a colleague, intern or friend that would be willing to try it out with you.
2. Set up a one hours zoom.
3. Set the agenda at the beginning of the call: a. 5 min brainstorm b. 45 min work c. 5 min brainstorm

You each spend an hour on a Zoom working on two different challenges -- things you would be working on anyway.
You can chose particularly difficult things that you've been procrastinating on for a while.
Or things that you could use a little outside perspective to help you break out of a mental block.
Sometimes it can help if you're both working on similar things, like maybe some python code, or maybe two data science problems.
Kaggle competitions work great.
Ideally you want a project that you should be able to accomplish something significant for in less than an hour.

At the beginning of the call, you talk about what you plan to accomplish and brainstorm about approaches for each of you.
At the end you do some more brainstorming.
Most importantly, you have to hold your power hour partner accountable for whatever they planned to focus on for the hour.
"Did you get it done?"
"What are your next steps?"

You can stay muted during the middle of the call to avoid distractions.
It's like having a workmate at the desk next to you.
Sometimes it can create a lot of good energy and generate new ideas and a more objective perspective.
It's a bit better than "rubber ducking".
It can also help with writer's block or difficulties focusing.
It's kind-of like Pomodoros, but with a bit of help from a friend.

## Future work

Context switching is a bear.
The human mind is incapable of multitasking without severely reducing your effectiveness and throughput.
You can only make so many good decisions during the day.
And your short term memory can only hold so many facts and ideas at once.
To minimize mental "switching costs" you can automate some of the things you need to do when you switch projects or tasks.
For me that means maintainin a 'workon' command line script.
When I say "workon qary" a shell script goes to work recording where I'm at and creating breadcrumbs for my future self:

1. commits and pushes my latest code
2. logs the the command line history for whatever I was just working on

If you're currently working on a software test that is failing you just need to remind yourself with a commit message or syntax error code comments that tell your future self how to run the test or what to look for in the output.

To get me primed for the next project my script:

1. opens the web pages I might need (including local files like the nlpia adoc files)
2. changes my python virtual environment to match the project
3. launches an IDE or editor with the project prepopulated with the folders I need open
4. launches jupyter notebook or any other apps I need
5. tails the command line history from the last time i worked on the project
6. runs a `git status` and `git log` to help me get oriented

If you're an author working on a writing project like a blog post, it's usually a good idea to leave your last thought unfinished.
At the end of one project and before starting on a different one, start an incomplete sentence so that your future self will have to fill in the blanks.
Just make sure you can find that grammatically broken sentence when you next pick up the text.
Just like in code, I use TODO: and FIXME: markers liberally throughout my text. 
Most IDEs will pay attention to these and help you find them.

You should chose a specific personalized marker that shows you the last thing you did 
