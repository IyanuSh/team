# Agile

Agile (capital "A") is a methodology for deciding on tasks and developing software on a team (even just a team of 2... you and your future self).
The key is to think about user and associate your tasks with "user stories", even if the user is your future self.

## References

- [Udemy Agile Crash Course & Certification](https://www.udemy.com/course/agile-crash-course/) ($25-$150)
- [Udemy Agile Methodololgy](https://www.udemy.com/course/agile-methodology-with-scrum/?LSNPUBID=0F1O0otUXQc&ranEAID=0F1O0otUXQc&ranMID=39197&ranSiteID=0F1O0otUXQc-L3VaPIParwvJyImmRKuV.A&utm_medium=udemyads&utm_source=aff-campaign) (free)
- [EdX BerkeleyX Intro to Agile Software Development](https://www.edx.org/course/introduction_to_agile_software_development?index=product&queryID=c82544e454e1c7d7ab8a3d3523e27d43&position=6)
