# Language models

This week you will learn about language models that enable machines read and understand human languages. You'll even build your own basic language model using `dict`ionaries and `for` loops.
