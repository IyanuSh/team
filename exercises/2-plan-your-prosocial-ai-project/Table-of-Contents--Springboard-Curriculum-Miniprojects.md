│   └── Practical Data Science Project.ipynb
├── README.md
├── Table-of-Contents--Springboard-Curriculum-Miniprojects.md
│   ├── AppleStore.csv
│   ├── Frequentist Inference Case Study - Part A (3).ipynb
│   ├── Frequentist Inference Case Study - Part B (2).ipynb
│   ├── googleplaystore.csv
│   ├── insurance2.csv
│   ├── Springboard Apps project - Tier 3 - Complete.ipynb
│   ├── Springboard Regression Case Study - the Red Wine Dataset - Tier 3.ipynb
│   └── wineQualityReds.csv
│   ├── __about__.py
│   ├── Gradient Boosting Case Study.ipynb
│   ├── Logistic Regression Advanced Case Study.ipynb
│   ├── RandomForest_casestudy_covid19.ipynb
│   ├── RRDinerCoffeeData.csv
│   ├── SouthKoreacoronavirusdataset-20200630T044816Z-001.zip
│   ├── Springboard Decision Tree Specialty Coffee Case Study - Tier 3.ipynb
│   └── titanic.csv
│   ├── Clustering Case Study - Customer Segmentation with K-Means - Tier 3.ipynb
│   ├── Cosine_Similarity_Case_Study.ipynb
│   ├── distance_dataset (1).csv
│   ├── distance_dataset.csv
│   ├── Euclidean_and_Manhattan_Distances_Case_Study.ipynb
│   ├── Bayesian_optimization_case_study.ipynb
│   ├── diabetes.csv
│   ├── flight_delays_test.csv.zip
│   ├── flight_delays_train.csv.zip
│   └── GridSearchKNN_Case_Study.ipynb
│   ├── Data Story.ipynb
│   ├── Cowboy Cigarettes Case Study - Tier 3.ipynb
│   └── CowboyCigsData.csv
│   ├── Challenge - Tier 3.ipynb
│   └── Solved.ipynb
│   ├── Data Wrangling .ipynb
│   ├── Exploratory Data Analysis.ipynb
│   ├── Modelling.ipynb
│   ├── Preprocessing Data.ipynb
│   ├── ski_data_cleaned.csv
│   ├── ski_data_step3_features.csv
│   ├── state_summary.csv
│   ├── API Mini-Project.ipynb
│   └── Pandas-Profiling.ipynb
    └── Case Study: Country Club.ipynb
