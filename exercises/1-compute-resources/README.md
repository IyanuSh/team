# Managing compute resources

When your laptop slows down or freezes you'll want to figure out the source of the problem.
If you can't use Jupyter Notebook or work with large files then it is not suitable for your software development and data science work.

## Memory (RAM)

Usually when your computer slows down it's because you've run out of memory (RAM).
When this happen your computer starts to SWAP data (files and applications) back and forth in and out of RAM and disk.
You can imagine how slow it is to write data to disk and then read it back into RAM every time it needs to work with a new bit of data.
And it even does this SWAPing with each new button click within the app you are using at the moment.

On Linux (and sometimes on Mac) you can use the `htop` application to monitor RAM and CPU usage in your terminal.

- Ubuntu: `sudo apt install htop`
- Mac: `brew install htop`
- Windows [git-bash chocolatey ntop](https://chocolatey.org/install): `choco install ntop.portable`
- Windows [git-bash pacman](https://github.com/git-for-windows/build-extra/releases): `pacman install htop`

#### Windows

Check out the [bash exercise](https://gitlab.com/tangibleai/team/-/tree/main/exercises/1-bash/README.md).
Look for the Windows instructions or any mention of chocolatey if you've never used git-bash on Windows before.

#### Linux (htop)

If you've installed htop or ntop in your Linux-like shell (bash or git-bash) you can get a nice overview of you resource usage in the terminal:

```bash
$ htop  # or ntop on Windows git-bash
```

![htop (ntop) shows your CPU RAM and Swap usage](htop-ubuntu-ram-cpu-swap.png)

#### Mac

On Mac you can launch your resource monitor app by typing F4 to launch your "Activity Monitor".
You can also find your monitor application in your Launcher by typing "monitor" within your Application Launch.
Within Mac Activity Monitor, look for a tab or window that shows you the percent (%) of RAM and CPU you are currently using.
Pay close attention to the SWAP %.
"Swap Used" should always be 0%.
And you should keep your RAM usage will below 90%.
Activity Monitor shows:

- *Physical Memory* - total GB of RAM installed in your computer
- *Memory Used* - total GB of RAM used by your running applications
- *Cached Files* - data files (documents, web pages) temporarily stored on disk
- *Swap Used* - amount of data (GB) from RAM being temporarily stored on disk

![Activity Monitor shows your *Physical Memory* (total RAM), *Memory Used*, *Cached Files*, and *Swap Used*](mac-activity-monitor-screenshot.png "Activity Monitor screenshot")

If you're using too much RAM you do some quick "garbage collection" to see if you can free up some RAM.
If you are using Chrome, you want to close it first.
It uses more RAM than any other application on your machine.
If you've set it as your default browser that is launched when you us Jupyter Notebook, then you can **[install Firefox](https://www.wikihow.com/Download-and-Install-Mozilla-Firefox)** for [Mac](https://www.wikihow.com/Download-and-Install-Mozilla-Firefox#Firefox-for-Mac) or [Windoze](https://www.wikihow.com/Download-and-Install-Mozilla-Firefox#Firefox-for-Windows). [Linux]() users are in luck. It's probably already installed. If not check out the [installation instructions from Mozilla](https://support.mozilla.org/en-US/kb/install-firefox-linux#firefox:linux).
As a bonus Firefox prevents ads from tracking you and gobbling up your resources with bloatware or even malware.

Firefox is much more efficient but you'll still need to limit the number of tabs you have open.
When you're working on a Jupyter notebook, make sure your browser only has tabs open that you absolutely need for doing your work.
To get familiar with the memory hogs on your system, check out the resource monitor each time you close another browser tab or application.
If you insist on using an antisocial browser like Chrome, then make sure you close everything except the Jupyter notebook tab.

## Disk Space (SSD Memory)

On Mac or Linux you can type `df` in a terminal to display the "disk free".

```bash
Filesystem     1K-blocks      Used Available Use% Mounted on
udev            32785652         0  32785652   0% /dev
tmpfs            6563824      2260   6561564   1% /run
/dev/nvme0n1p2 771450868 198089256 534104352  28% /
tmpfs           32819116       476  32818640   1% /dev/shm
tmpfs               5120         4      5116   1% /run/lock
tmpfs           32819116         0  32819116   0% /sys/fs/cgroup
/dev/nvme0n1p1    523248      5360    517888   2% /boot/efi
tmpfs            6563820       128   6563692   1% /run/user/1000
```

## Processors (CPU cores)
