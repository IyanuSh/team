Both the Rasa and `qary` chatbot frameworks use yaml files to define the state transitions. Mathematicians use a concept called Finite State Machine (FSM) to reason about software like this that has state.


Rochdi describes FSMs like this:

- It is a computational model that consists of one or more states
- It can be only in single active state at any given time
- An input to the machine causes a transition to the next state
- Each state defines the state to switch to based on the input

Finite State Machine (FSM) is a theoretical mathematics term for systems that manage state and state transitions. The `qary` and `rasa` chatbot frameworks have a yaml file syntax they use to define how the internal FSM should work. It's like a programming language for a particular kind of FSM that takes text input from the user and outputs text after each transition to a new state.

Can you think of another "programming language" designed to take in text and transition to a new state with each new text?
What if the "text" input was a single character?
Now can you guess what this programming language or syntax is called?

Every regular expression you write also defines a finite state machine.
The pattern argument to the python `re.compile(pattern)` function is a regular expression that defines a FSM.
The first argument to a POSIX `grep` command is a regular expression that defines a FSM with almost the exact same syntax as python `re.compile()` and `re.match()`.
If you need more complicated state machines for matching text in a "fuzzy" (probabilistic) way, you can use the python `regex` package to define your state machine.
And if you need something even more flexible to define **any** finite state machine you can use the `pynini` package maintained by Kyle Gorman.

## FSMS as Thinking Tools

FSMS are a great way for programmers to visualize and reason about about a lot of computer algorithms. This tutorial at `gamedev.net` provides some examples: [finite-state-machines-and-regular-expressions](https://gamedev.net/tutorials/_/technical/general-programming/finite-state-machines-and-regular-expressions-r3176/).

### Python Packages for Creating FSMs

- [PyNini](https://pypi.org/project/pynini/)
- [regex](https://pypi.org/project/regex/)
- [re](https://docs.python.org/3/library/re.html)
