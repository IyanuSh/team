```python
import pandas as pd

pd.options.display.max_columns = 1000
```


```python
pd.read_html("https://simple.m.wikipedia.org/wiki/List_of_countries_by_area")
```




    [    Pos Country             Total in km2 (mi2)              Unnamed: 2
     0             1                         Russia  17,098,246 (6,601,670)
     1             2                         Canada   9,984,670 (3,855,100)
     2             3                          China   9,596,961 (3,705,407)
     3             4                  United States   9,525,067 (3,677,649)
     4             5                         Brazil   8,515,767 (3,287,956)
     ..          ...                            ...                     ...
     253           –            Clipperton (France)                12 (4.6)
     254           –     Spratly Islands (disputed)                 5 (1.9)
     255           –  Coral Sea Islands (Australia)                 3 (1.2)
     256         193                         Monaco             2.02 (0.78)
     257         194                   Vatican City             0.44 (0.17)
     
     [258 rows x 3 columns]]




```python
!pip install lxml
```

    Requirement already satisfied: lxml in /home/hobs/anaconda3/envs/dsc/lib/python3.7/site-packages (4.6.3)



```python
pd.read_html("https://simple.m.wikipedia.org/wiki/List_of_countries_by_area")
```




    [    Pos Country             Total in km2 (mi2)              Unnamed: 2
     0             1                         Russia  17,098,246 (6,601,670)
     1             2                         Canada   9,984,670 (3,855,100)
     2             3                          China   9,596,961 (3,705,407)
     3             4                  United States   9,525,067 (3,677,649)
     4             5                         Brazil   8,515,767 (3,287,956)
     ..          ...                            ...                     ...
     253           –            Clipperton (France)                12 (4.6)
     254           –     Spratly Islands (disputed)                 5 (1.9)
     255           –  Coral Sea Islands (Australia)                 3 (1.2)
     256         193                         Monaco             2.02 (0.78)
     257         194                   Vatican City             0.44 (0.17)
     
     [258 rows x 3 columns]]




```python
dfs = _
for i, df in enumerate(dfs):
    print(f'df {i} shape: {df.shape}')
```

    0
    (258, 3)
    df 0 shape: (258, 3)



```python
df = dfs[0]
df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Pos Country</th>
      <th>Total in km2 (mi2)</th>
      <th>Unnamed: 2</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>Russia</td>
      <td>17,098,246 (6,601,670)</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2</td>
      <td>Canada</td>
      <td>9,984,670 (3,855,100)</td>
    </tr>
    <tr>
      <th>2</th>
      <td>3</td>
      <td>China</td>
      <td>9,596,961 (3,705,407)</td>
    </tr>
    <tr>
      <th>3</th>
      <td>4</td>
      <td>United States</td>
      <td>9,525,067 (3,677,649)</td>
    </tr>
    <tr>
      <th>4</th>
      <td>5</td>
      <td>Brazil</td>
      <td>8,515,767 (3,287,956)</td>
    </tr>
  </tbody>
</table>
</div>




```python
# It looks like it messed up the column headers. `Pos` and `Country` should be separate columns
```


```python
# Rename the columns to use your naming convention so you can remember what's what
# Think ahead and be as explicit as possible about your names
# Use underscores if you like to use the attribute approach (dot syntax) to access columns rather than label lookup (square brackets)
df.columns = ['area_rank', 'name', 'area (km^2 and mile^2)']
df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>area_rank</th>
      <th>name</th>
      <th>area (km^2 and mile^2)</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>Russia</td>
      <td>17,098,246 (6,601,670)</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2</td>
      <td>Canada</td>
      <td>9,984,670 (3,855,100)</td>
    </tr>
    <tr>
      <th>2</th>
      <td>3</td>
      <td>China</td>
      <td>9,596,961 (3,705,407)</td>
    </tr>
    <tr>
      <th>3</th>
      <td>4</td>
      <td>United States</td>
      <td>9,525,067 (3,677,649)</td>
    </tr>
    <tr>
      <th>4</th>
      <td>5</td>
      <td>Brazil</td>
      <td>8,515,767 (3,287,956)</td>
    </tr>
  </tbody>
</table>
</div>




```python
# let's see what type of data is in each column (dtype)
df.info()
```

    <class 'pandas.core.frame.DataFrame'>
    RangeIndex: 258 entries, 0 to 257
    Data columns (total 3 columns):
     #   Column                  Non-Null Count  Dtype 
    ---  ------                  --------------  ----- 
     0   area_rank               258 non-null    object
     1   name                    258 non-null    object
     2   area (km^2 and mile^2)  258 non-null    object
    dtypes: object(3)
    memory usage: 6.2+ KB



```python
# So we need to convert the rank number to an integer
df['area_rank'].astype(int)
```


    ---------------------------------------------------------------------------

    ValueError                                Traceback (most recent call last)

    <ipython-input-10-b65b7c3f3ec1> in <module>
          1 # So we need to convert the rank number to an integer
    ----> 2 df['area_rank'].astype(int)
    

    ~/anaconda3/envs/dsc/lib/python3.7/site-packages/pandas/core/generic.py in astype(self, dtype, copy, errors)
       5875         else:
       5876             # else, only a single dtype is given
    -> 5877             new_data = self._mgr.astype(dtype=dtype, copy=copy, errors=errors)
       5878             return self._constructor(new_data).__finalize__(self, method="astype")
       5879 


    ~/anaconda3/envs/dsc/lib/python3.7/site-packages/pandas/core/internals/managers.py in astype(self, dtype, copy, errors)
        629         self, dtype, copy: bool = False, errors: str = "raise"
        630     ) -> "BlockManager":
    --> 631         return self.apply("astype", dtype=dtype, copy=copy, errors=errors)
        632 
        633     def convert(


    ~/anaconda3/envs/dsc/lib/python3.7/site-packages/pandas/core/internals/managers.py in apply(self, f, align_keys, ignore_failures, **kwargs)
        425                     applied = b.apply(f, **kwargs)
        426                 else:
    --> 427                     applied = getattr(b, f)(**kwargs)
        428             except (TypeError, NotImplementedError):
        429                 if not ignore_failures:


    ~/anaconda3/envs/dsc/lib/python3.7/site-packages/pandas/core/internals/blocks.py in astype(self, dtype, copy, errors)
        671             vals1d = values.ravel()
        672             try:
    --> 673                 values = astype_nansafe(vals1d, dtype, copy=True)
        674             except (ValueError, TypeError):
        675                 # e.g. astype_nansafe can fail on object-dtype of strings


    ~/anaconda3/envs/dsc/lib/python3.7/site-packages/pandas/core/dtypes/cast.py in astype_nansafe(arr, dtype, copy, skipna)
       1072         # work around NumPy brokenness, #1987
       1073         if np.issubdtype(dtype.type, np.integer):
    -> 1074             return lib.astype_intsafe(arr.ravel(), dtype).reshape(arr.shape)
       1075 
       1076         # if we have a datetime/timedelta array of objects


    pandas/_libs/lib.pyx in pandas._libs.lib.astype_intsafe()


    ValueError: invalid literal for int() with base 10: '–'



```python
# a superstar tests their hypotheses
int('-')
int('–')
```


    ---------------------------------------------------------------------------

    ValueError                                Traceback (most recent call last)

    <ipython-input-12-6053b9ad7ee1> in <module>
          1 # a superstar tests their hypotheses
    ----> 2 int('-')
          3 int('–')


    ValueError: invalid literal for int() with base 10: '-'



```python
# Notice anything different about the last line of the two error messages?




```


```python
# So that's why it didn't create integers when it first loaded the DataFrame
# Let's write a function to clean it up
def convert_type(x): # never use a builtin (such as `type`) as a variable name
    if x == '–':
        return None
    else:
        return int(x)
```


```python
df['area_rank'] = df['area_rank'].apply(convert_type)
```


    ---------------------------------------------------------------------------

    ValueError                                Traceback (most recent call last)

    <ipython-input-23-42530bf2a320> in <module>
    ----> 1 df['area_rank'] = df['area_rank'].apply(convert_type)
    

    ~/anaconda3/envs/dsc/lib/python3.7/site-packages/pandas/core/series.py in apply(self, func, convert_dtype, args, **kwds)
       4136             else:
       4137                 values = self.astype(object)._values
    -> 4138                 mapped = lib.map_infer(values, f, convert=convert_dtype)
       4139 
       4140         if len(mapped) and isinstance(mapped[0], Series):


    pandas/_libs/lib.pyx in pandas._libs.lib.map_infer()


    <ipython-input-22-da0b5d516202> in convert_type(x)
          5         return None
          6     else:
    ----> 7         return int(x)
    

    ValueError: cannot convert float NaN to integer



```python
# Can you think of a better way?
# hint: python functions return None by default
# even better: handle other nonnumerical strings with regex or better string processing
# even better: "duck typing", isintance is used to detect types
# most pythonic: "ask for forgiveness"
```


```python
# hint: python functions return None by default
def convert_type(x, typ=int): # never use a builtin (such as `type`) as a variable name
    if x != '-'
        return int(x)
```


```python
# hint: python functions return None by default
def convert_type(x, typ=int): # never use a builtin (such as `type`) as a variable name
    try:
        return float(x)
    except:
        return float('nan')
```


```python
df['area_rank'] = df['area_rank'].apply(convert_type)
df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>area_rank</th>
      <th>name</th>
      <th>area (km^2 and mile^2)</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1.0</td>
      <td>Russia</td>
      <td>17,098,246 (6,601,670)</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2.0</td>
      <td>Canada</td>
      <td>9,984,670 (3,855,100)</td>
    </tr>
    <tr>
      <th>2</th>
      <td>3.0</td>
      <td>China</td>
      <td>9,596,961 (3,705,407)</td>
    </tr>
    <tr>
      <th>3</th>
      <td>4.0</td>
      <td>United States</td>
      <td>9,525,067 (3,677,649)</td>
    </tr>
    <tr>
      <th>4</th>
      <td>5.0</td>
      <td>Brazil</td>
      <td>8,515,767 (3,287,956)</td>
    </tr>
  </tbody>
</table>
</div>




```python
twocols = list(zip(*df['area (km^2 and mile^2)'].str.split()))
```


```python
df['area_sqkm'] = twocols[0]
df['area_sqmi'] = twocols[1]
```


```python
# hint: python functions return None by default
# even better: isintance is used to detect types
```


```python
df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>area_rank</th>
      <th>name</th>
      <th>area (km^2 and mile^2)</th>
      <th>area_sqkm</th>
      <th>area_sqmi</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1.0</td>
      <td>Russia</td>
      <td>17,098,246 (6,601,670)</td>
      <td>17,098,246</td>
      <td>(6,601,670)</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2.0</td>
      <td>Canada</td>
      <td>9,984,670 (3,855,100)</td>
      <td>9,984,670</td>
      <td>(3,855,100)</td>
    </tr>
    <tr>
      <th>2</th>
      <td>3.0</td>
      <td>China</td>
      <td>9,596,961 (3,705,407)</td>
      <td>9,596,961</td>
      <td>(3,705,407)</td>
    </tr>
    <tr>
      <th>3</th>
      <td>4.0</td>
      <td>United States</td>
      <td>9,525,067 (3,677,649)</td>
      <td>9,525,067</td>
      <td>(3,677,649)</td>
    </tr>
    <tr>
      <th>4</th>
      <td>5.0</td>
      <td>Brazil</td>
      <td>8,515,767 (3,287,956)</td>
      <td>8,515,767</td>
      <td>(3,287,956)</td>
    </tr>
  </tbody>
</table>
</div>




```python
df['area_sqkm'].str.replace(',', '_')
```




    0      17_098_246
    1       9_984_670
    2       9_596_961
    3       9_525_067
    4       8_515_767
              ...    
    253            12
    254             5
    255             3
    256          2.02
    257          0.44
    Name: area_sqkm, Length: 258, dtype: object




```python
df['area_sqkm'] = (df['area_sqkm'].str.replace(',', '_')).astype(float)
```


```python
df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>area_rank</th>
      <th>name</th>
      <th>area (km^2 and mile^2)</th>
      <th>area_sqkm</th>
      <th>area_sqmi</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1.0</td>
      <td>Russia</td>
      <td>17,098,246 (6,601,670)</td>
      <td>17098246.0</td>
      <td>(6,601,670)</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2.0</td>
      <td>Canada</td>
      <td>9,984,670 (3,855,100)</td>
      <td>9984670.0</td>
      <td>(3,855,100)</td>
    </tr>
    <tr>
      <th>2</th>
      <td>3.0</td>
      <td>China</td>
      <td>9,596,961 (3,705,407)</td>
      <td>9596961.0</td>
      <td>(3,705,407)</td>
    </tr>
    <tr>
      <th>3</th>
      <td>4.0</td>
      <td>United States</td>
      <td>9,525,067 (3,677,649)</td>
      <td>9525067.0</td>
      <td>(3,677,649)</td>
    </tr>
    <tr>
      <th>4</th>
      <td>5.0</td>
      <td>Brazil</td>
      <td>8,515,767 (3,287,956)</td>
      <td>8515767.0</td>
      <td>(3,287,956)</td>
    </tr>
  </tbody>
</table>
</div>




```python
# You can move the country name to the index if you want to be able to join this tables with other tadfbles
df = df.set_index(['name'], drop=False)
df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>area_rank</th>
      <th>name</th>
      <th>area (km^2 and mile^2)</th>
      <th>area_sqkm</th>
      <th>area_sqmi</th>
    </tr>
    <tr>
      <th>name</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>Russia</th>
      <td>1.0</td>
      <td>Russia</td>
      <td>17,098,246 (6,601,670)</td>
      <td>17098246.0</td>
      <td>(6,601,670)</td>
    </tr>
    <tr>
      <th>Canada</th>
      <td>2.0</td>
      <td>Canada</td>
      <td>9,984,670 (3,855,100)</td>
      <td>9984670.0</td>
      <td>(3,855,100)</td>
    </tr>
    <tr>
      <th>China</th>
      <td>3.0</td>
      <td>China</td>
      <td>9,596,961 (3,705,407)</td>
      <td>9596961.0</td>
      <td>(3,705,407)</td>
    </tr>
    <tr>
      <th>United States</th>
      <td>4.0</td>
      <td>United States</td>
      <td>9,525,067 (3,677,649)</td>
      <td>9525067.0</td>
      <td>(3,677,649)</td>
    </tr>
    <tr>
      <th>Brazil</th>
      <td>5.0</td>
      <td>Brazil</td>
      <td>8,515,767 (3,287,956)</td>
      <td>8515767.0</td>
      <td>(3,287,956)</td>
    </tr>
  </tbody>
</table>
</div>




```python
dfs = pd.read_html('https://en.wikipedia.org/wiki/World_Happiness_Report')
len(dfs)
```




    20




```python
for i, happy in enumerate(dfs):
    print(f'happy {i} shape: {happy.shape}')

```

    happy 0 shape: (1, 1)
    happy 1 shape: (1, 1)
    happy 2 shape: (1, 1)
    happy 3 shape: (1, 1)
    happy 4 shape: (1, 1)
    happy 5 shape: (1, 1)
    happy 6 shape: (1, 1)
    happy 7 shape: (1, 1)
    happy 8 shape: (155, 9)
    happy 9 shape: (153, 9)
    happy 10 shape: (158, 9)
    happy 11 shape: (156, 9)
    happy 12 shape: (158, 9)
    happy 13 shape: (156, 9)
    happy 14 shape: (159, 12)
    happy 15 shape: (157, 12)
    happy 16 shape: (160, 10)
    happy 17 shape: (1, 2)
    happy 18 shape: (157, 10)
    happy 19 shape: (6, 2)



```python
dfs[8]
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Table</th>
      <th>Unnamed: 1</th>
      <th>Unnamed: 2</th>
      <th>Unnamed: 3</th>
      <th>Unnamed: 4</th>
      <th>Unnamed: 5</th>
      <th>Unnamed: 6</th>
      <th>Unnamed: 7</th>
      <th>Unnamed: 8</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>Overall rank Country or region Score GDP per c...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>1</th>
      <td>Overall rank</td>
      <td>Country or region</td>
      <td>Score</td>
      <td>GDP per capita</td>
      <td>Social support</td>
      <td>Healthy life expectancy</td>
      <td>Freedom to make life choices</td>
      <td>Generosity</td>
      <td>Perceptions of corruption</td>
    </tr>
    <tr>
      <th>2</th>
      <td>1</td>
      <td>Finland</td>
      <td>7.809</td>
      <td>1.285</td>
      <td>1.500</td>
      <td>0.961</td>
      <td>0.662</td>
      <td>0.160</td>
      <td>0.478</td>
    </tr>
    <tr>
      <th>3</th>
      <td>2</td>
      <td>Denmark</td>
      <td>7.646</td>
      <td>1.327</td>
      <td>1.503</td>
      <td>0.979</td>
      <td>0.665</td>
      <td>0.243</td>
      <td>0.495</td>
    </tr>
    <tr>
      <th>4</th>
      <td>3</td>
      <td>Switzerland</td>
      <td>7.560</td>
      <td>1.391</td>
      <td>1.472</td>
      <td>1.041</td>
      <td>0.629</td>
      <td>0.269</td>
      <td>0.408</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>150</th>
      <td>149</td>
      <td>Central African Republic</td>
      <td>3.476</td>
      <td>0.041</td>
      <td>0.000</td>
      <td>0.000</td>
      <td>0.293</td>
      <td>0.254</td>
      <td>0.028</td>
    </tr>
    <tr>
      <th>151</th>
      <td>150</td>
      <td>Rwanda</td>
      <td>3.312</td>
      <td>0.343</td>
      <td>0.523</td>
      <td>0.572</td>
      <td>0.604</td>
      <td>0.236</td>
      <td>0.486</td>
    </tr>
    <tr>
      <th>152</th>
      <td>151</td>
      <td>Zimbabwe</td>
      <td>3.299</td>
      <td>0.426</td>
      <td>1.048</td>
      <td>0.375</td>
      <td>0.377</td>
      <td>0.151</td>
      <td>0.081</td>
    </tr>
    <tr>
      <th>153</th>
      <td>152</td>
      <td>South Sudan</td>
      <td>2.817</td>
      <td>0.289</td>
      <td>0.553</td>
      <td>0.209</td>
      <td>0.066</td>
      <td>0.210</td>
      <td>0.111</td>
    </tr>
    <tr>
      <th>154</th>
      <td>153</td>
      <td>Afghanistan</td>
      <td>2.567</td>
      <td>0.301</td>
      <td>0.356</td>
      <td>0.266</td>
      <td>0.000</td>
      <td>0.135</td>
      <td>0.001</td>
    </tr>
  </tbody>
</table>
<p>155 rows × 9 columns</p>
</div>




```python
happy = dfs[8]
columns = happy.iloc[0]['Table']
len(columns), happy.shape 
```




    (8646, (155, 9))




```python
columns = 'Overall rank, Country or region, Score, GDP per capita, Social support, Healthy life expectancy, Freedom to make life choices, Generosity, Perceptions of corruption'
columns = list((c.strip() for c in columns.split(',')))
len(list(columns))
```




    9




```python

```


```python
happy.columns = columns
happy.describe()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Overall rank</th>
      <th>Country or region</th>
      <th>Score</th>
      <th>GDP per capita</th>
      <th>Social support</th>
      <th>Healthy life expectancy</th>
      <th>Freedom to make life choices</th>
      <th>Generosity</th>
      <th>Perceptions of corruption</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>count</th>
      <td>155</td>
      <td>154</td>
      <td>154</td>
      <td>154</td>
      <td>154</td>
      <td>154</td>
      <td>154</td>
      <td>154</td>
      <td>154</td>
    </tr>
    <tr>
      <th>unique</th>
      <td>155</td>
      <td>154</td>
      <td>151</td>
      <td>140</td>
      <td>141</td>
      <td>122</td>
      <td>136</td>
      <td>129</td>
      <td>116</td>
    </tr>
    <tr>
      <th>top</th>
      <td>8</td>
      <td>Palestine</td>
      <td>5.546</td>
      <td>1.327</td>
      <td>1.459</td>
      <td>1.023</td>
      <td>0.435</td>
      <td>0.170</td>
      <td>0.087</td>
    </tr>
    <tr>
      <th>freq</th>
      <td>1</td>
      <td>1</td>
      <td>2</td>
      <td>3</td>
      <td>2</td>
      <td>4</td>
      <td>3</td>
      <td>4</td>
      <td>4</td>
    </tr>
  </tbody>
</table>
</div>




```python
# You probably your index to be the rank order number (starting at 1 instead of 0)?
df = pd.read_html("https://simple.m.wikipedia.org/wiki/List_of_countries_by_area", index_col=0)[0]
df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Total in km2 (mi2)</th>
      <th>Unnamed: 2</th>
    </tr>
    <tr>
      <th>Pos Country</th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <td>Russia</td>
      <td>17,098,246 (6,601,670)</td>
    </tr>
    <tr>
      <th>2</th>
      <td>Canada</td>
      <td>9,984,670 (3,855,100)</td>
    </tr>
    <tr>
      <th>3</th>
      <td>China</td>
      <td>9,596,961 (3,705,407)</td>
    </tr>
    <tr>
      <th>4</th>
      <td>United States</td>
      <td>9,525,067 (3,677,649)</td>
    </tr>
    <tr>
      <th>5</th>
      <td>Brazil</td>
      <td>8,515,767 (3,287,956)</td>
    </tr>
  </tbody>
</table>
</div>


