# Serverless

Web applications and APIs without managing or provisioning services.  Imagine `docker run` in the cloud.

## Advantages

- incremental cost (low cost for low traffic)
- security
- asynchronous (automatically scaled)

## Disadvantages

- slow startup
- complicated deployment of long-running (machine learning) tasks
- vendor/service lock-in

## Serverless Frameworks

- Serverless Framework
    - node.js
- Chalice
    - python
- Zappa:
    - python
    - open source
    - AWS Lambda only?
- AWS SAM Templates
    - proprietary language

## Serverless Providers

- Apache OpenWhisk
- Open FaaS (self-hosted Docker Swarm)
- AWS Lambda
- Google Cloud Functions
- Axure Functions
