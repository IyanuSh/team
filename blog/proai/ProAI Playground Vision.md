# ProAI Pitch

A game that makes learning STEM and AI Engineering fun.

- play games that teach you about linear regression, logistic regression and A* search
- build a chatbot in python (v3.yaml)
- build bots in python that play tic tac toe, then chess, then RPGs.
- build therapy/companion chatbots
- build research assistant bots

## Goals

- incorporate as nonprofit
- apply for Schmidt Futures grant []()
- 3 levels in playground.proai.org
- working donation button
- patreon?
- kickstarter?
- regular (monthly) blog posts
- qary version of `nudger` on render.com
- chat scripts for handling intern onboarding
- playground on render.com
- gitpod instance on render.com

## Backlog

- search engine that uses meillisearch (search.qary.ai)
- drawio instance on render.com
