# Data Science Curriculum

## What makes proai.org unique:

- Better capstone projects
- Engineers, not scientists
- Better learning engineering
- Adaptive, personalized curriculum
- Data driven (accountability)
- Employer partnerships
- Inspiring (social impact) projects and jobs

## Better capstone projects

- Students work through real problems in a guided way
- Delay business problem solving until real world examples are complete.
- The Acme Ski Resort company or Mining Company examples of Springboard are bad
- The rhyming poetry chatbot example at Thinkful is good

## Engineer != Data Scientist

- Train Python Developer + Machine Learning Engineer
- Software development taught first
- Computational thinking, not copypasta
-

## Better Learning Engineering

- active Learning
- meta cognition teaching
- encourage mistakes
- build on skills already mastered
- no busy work, no copy/paste, all code snippets have all variables defined
- quizzes as for valid python code
- REPL (ipython) for most work

## Best-practice Industrial tools & processes

- python
- django
- Linux
- git
- gitlab
- ssh
- docker
- all FOSS

## Best-practice workflows

- Agile, standups
- unittests, TDD
- doctests, DDD
- CI/CD, .gitlab-ci
- docker-compose

## Resume-building

- Contributions to popular open source projects (Rasa, Scikit-Learn, iPython, Qary, NLPIA) appropriate to skill level
- Internship title more impressive that code-school
- Work on real world customer projects/data
- Reputation for more practical skills.

## Job Guarantee

- Partnerships where some students guaranteed placement on small contract
-

## Data driven

- Quantifitative Performance Feedback
-

## Potential Partners

### Curriculum Cross-posting of Curriculum/Blogs

- [San Diego Tech Hub -- AI for Everyone course]()
- [David's Python Morsels course]()
- [UCSD Extension -- DSDH, DH, & Dian's Python 101]()
- [Educative](educative.io)
- [Springboard](springboard.com)

### Curriculum Marketing

- [Medium - Towards DS](medium.com)
- []()

### Curriculum Cross-posting Ideas Backlog

- [Data Camnp](https://www.datacamp.com/)
- [Kahn Academy](https://www.khanacademy.org/)
- [Thinkful](thinkful.com)
- [Udacity](udacity.com)
- [Coursera](coursera.org)
- [EdX](edx.org)
- [Kahn Academy]

### Detractors

- [promotes AWS products](https://www.edequity.global/)
