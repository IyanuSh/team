# Dimagi CommCare links

- [CommCare API](https://confluence.dimagi.com/display/commcarepublic/CommCare+HQ+APIs)
- [CommCare Messaging](https://confluence.dimagi.com/display/commcarepublic/CommCare+Messaging)
- [Self registration](https://confluence.dimagi.com/display/commcarepublic/SMS+Self+Registration)
- SMS Surveys: https://confluence.dimagi.com/display/commcarepublic/Setup+an+SMS+Survey
- Broadcasts: https://confluence.dimagi.com/display/commcarepublic/Broadcast
- Schedules (messaging campaigns): https://confluence.dimagi.com/display/commcarepublic/Custom+Schedules
- Conditional alerts (for pre-visit reminders): https://confluence.dimagi.com/display/commcarepublic/Conditional+Alert+Use+Cases
